/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        'green': '#03AC0E',
        'grey': '#F5F5F5',
        'dark-grey' : '#7A7A7A',
        'light-grey' : '#c8c8c8',
        'red' : '#F62845',
        'dark-green' : '#049C0E',
        'light-green' : 'rgba(3, 172, 14, 0.22)',
        'green-shadow' : 'rgba(3, 172, 14, 0.24)'
      }
    },
  },
  plugins: [],
}
