import { createRouter, createWebHistory } from 'vue-router'

import Login from '../components/Login/Login.vue'
import Register from '../components/Register/Register.vue';
import VerifikasiOTP from '../components/OTP/OTP.vue'
import SuccessLogin from '../components/SuccessLogin/SuccessLogin.vue';
import Category from '../components/Category/Category.vue';
import Product from '@/components/Product/Product.vue';
import SingleProduct from '@/components/SingleProduct/SingleProduct.vue';
import Cart from '@/components/Cart/Cart.vue';

const routes = [
    { path: '/', component: Login },
    { path: '/Login', component: Login },
    { path: '/Register', component: Register},
    { path: '/VerifikasiOTP', component: VerifikasiOTP},
    { path: '/SuccessLogin', component: SuccessLogin},
    { path: '/Category', component: Category},
    { path: '/Category/:category', component: Product},
    { path: '/Category/:category/:id', component: SingleProduct},
    { path: '/Cart', component: Cart},

]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;