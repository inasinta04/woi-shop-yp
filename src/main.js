import { createApp } from 'vue'
import App from './App.vue'
import './assets/tailwind.css'
import router from './router'
import store from './store'
import Vuex from 'vuex'
import './interceptors/axios'

createApp(App)
    .use(router)
    .use(store)
    .use(Vuex)
    .mount('#app')
